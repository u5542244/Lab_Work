package Shape;




public class DemoTextDraw {
	public static void main(String[] args){
		  Shape box = new Box();
		   Shape tri = new Triangle();
		   box.draw();
		   System.out.println("-------------------");
		   tri.draw();
	}

}

// IN problem2, we used interface; IN problem4 we used abstract class
//Talking about the difference between interface and abstract class:
// 1. one class can only extend one abstract class, however one class can implement several interfaces
// 2. abstract class can contain normal methods, however interface can only have abstract method
// 3. abstract class can contain static methods, however interface can not contain static method


