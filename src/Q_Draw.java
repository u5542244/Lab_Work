
public class Q_Draw {
          public static void draw (int n) {

        for (int i = 0; i < n; i++) {
            int t =0; //t is the number of spaces we needed 
            if (i >= n / 2) {
                t = n - 1 - i;
            }else{
                t = i;
            }
            for (int j = 1; j <= n; j++) {
                if (j >= (n + 1) / 2 - t && j <= (n + 1) / 2 + t) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }

}
          public static void main(String[] args){
        	  draw(15);
        	  
          }
}
